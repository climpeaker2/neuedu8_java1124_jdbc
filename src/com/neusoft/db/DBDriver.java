package com.neusoft.db;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.neusoft.bean.EmpBean;
import com.neusoft.myUtils.MyUtil;

public class DBDriver {

	/**
	 * 获取数据库连接对象
	 * 
	 * @return 数据库连接对象
	 */
	public Connection getConn() {
		Connection conn = null;
		try {
			// 1、注册数据库驱动
			// 数据库驱动类型：数据库驱动类的包路径名
			//String dBDriver = "oracle.jdbc.driver.OracleDriver";
			// 数据库服务器的访问连接地址：ip地址可以是localhost、127.0.0.1、ip地址
			//String URL = "jdbc:oracle:thin:@10.25.128.166:1521:orcl";
			// 用户名和密码
			//String userName = "scott";
			//String password = "tiger";
			//通过读取属性配置文件，读取数据库连接串的参数值
			//要求数据库属性配置文件直接放在src的根目录下
			InputStream is = this.getClass().getClassLoader()
					.getResourceAsStream("db.properties");
			// 使用工具类properties类加载这个数据属性文件输入流
			Properties prop = new Properties();
			prop.load(is);
			//获取数据库连接参数值
			String URL = prop.getProperty("URL");
			String userName = prop.getProperty("userName");
			String password = prop.getProperty("password");
			// Class.forName(dBDriver).newInstance();
			// 2、获取连接对象
			conn = DriverManager.getConnection(URL, userName, password);
		} catch (Exception e) {
			// 异常处理
			System.out.println("获取数据库连接异常：" + e.getMessage());
			e.printStackTrace();
		}
		return conn;
	}
	
	/**
	 * 数据库查询操作
	 * 
	 * @param strSql
	 *            查询sql语句
	 * @return 返回结果集resultset
	 */
	public ResultSet doQuery(String strSql) {
		// 默认返回值
		ResultSet rs = null;
		try {
			// 3、使用连接对象创建一个查询对象
			Statement st = getConn().createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			// 4、组织处理业务逻辑的sql语句
			// 6、执行sql语句获取查询结果
			rs = st.executeQuery(strSql);
		} catch (SQLException e) {
			// 数据库查询操作异常
			System.out.println("数据库查询操作异常:" + e.getMessage());
			e.printStackTrace();
		}
		return rs;
	}

	/**
	 * 数据库查询操作
	 * 
	 * @param strSql
	 *            查询sql语句
	 * @return 返回结果集ListMap
	 */
	public List<Map<String, Object>> doQueryListMap(String strSql) {
		// 执行查询操作
		ResultSet rs = doQuery(strSql);
		// 将查询结果集转换为ListMap对象
		List<Map<String, Object>> lsts = MyUtil.convertResultSet2List(rs);
		// 关闭结果集
		this.close(rs);
		return lsts;
	}
	/**
	 * 数据库查询操作
	 * 
	 * @param strSql
	 *            查询sql语句
	 * @return 返回结果集ListBean
	 */
	public List<EmpBean> doQueryListBean(String strSql) {
		// 执行查询操作
		ResultSet rs = doQuery(strSql);
		// 将查询结果集转换为ListMap对象
		List<EmpBean> lsts = MyUtil.convertResultSet2ListBean(rs);
		// 关闭结果集
		this.close(rs);
		return lsts;
	}
	/**
	 * 数据库查询操作
	 * @param <T>
	 * 
	 * @param strSql
	 *            查询sql语句
	 * @return 返回结果集ListBean
	 */
	public <T> List<T> doQueryListBean(String strSql,Class<T> tClazz) {
		// 执行查询操作
		ResultSet rs = doQuery(strSql);
		// 将查询结果集转换为ListMap对象
		List<T> lsts = MyUtil.convertResultSet2ListBean(rs,tClazz);
		// 关闭结果集
		this.close(rs);
		return lsts;
	}
	/**
	 * 数据库修改操作（可以执行update/delete/insert/ddl语句）
	 * 
	 * @param strSql
	 *            执行的sql语句
	 * @return 返回执行结果（影响的记录条数）
	 */
	public int doUpdate(String strSql) {
		// 默认返回值
		int re_i = 0;
		try {
			// 3、使用连接对象创建一个查询对象
			Statement st = getConn().createStatement();
			// 4、组织处理业务逻辑的sql语句
			// 6、执行sql语句获取查询结果
			re_i = st.executeUpdate(strSql);
		} catch (SQLException e) {
			// 数据库查询操作异常
			System.out.println("数据库修改操作异常:" + e.getMessage());
			e.printStackTrace();
		}
		return re_i;
	}

	/**
	 * 关闭数据连接
	 * 
	 * @param conn
	 */
	public void close(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			// 关闭数据库连接异常
			e.printStackTrace();
		}
	}
	/**
	 * 关闭数据连接
	 * 
	 * @param conn
	 */
	public void close(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			// 关闭数据库连接异常
			e.printStackTrace();
		}
	}
}
