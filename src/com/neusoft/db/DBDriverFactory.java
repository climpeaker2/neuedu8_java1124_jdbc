package com.neusoft.db;

public class DBDriverFactory {

	/**
	 * DBDriver工厂类
	 * 
	 * @return DBDriver实例对象
	 */
	public static DBDriver getDBDriverInstance() {
		return new DBDriver();
	}
}
