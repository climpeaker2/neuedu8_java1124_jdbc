package com.neusoft.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.neusoft.bean.EmpBean;
import com.neusoft.db.DBDriverFactory;

/*
 * java使用jdbc组件连接oracle数据库：
 * 1、注册数据库驱动
 * 2、获取连接对象
 * 3、使用连接对象创建一个查询对象
 * 4、组织处理业务逻辑的sql语句
 * 6、执行sql语句获取查询结果
 * 7、解析查询结果集
 * 8、关闭资源
 * 开发过程：
 * 1、将数据库驱动包拷贝到web项目的webcontent目录下的web-info下的lib目录（第三方的jar包全部放到lib目录下）
 *    oralce的数据库驱动包在Oracle的安装目录下：
 *    D:\app\Administrator\product\11.2.0\dbhome_1\jdbc\lib\ojdbc6.jar
 * 2、检查是否引入数据库驱动包（如果直接复制到lib目录下，会自动引入，需要确认检查一下）；
 *   在项目的libraries目录下可以看到该包的引用；
 * 3、如果没有自动引入，可以通过一下途径手动将lib包引入项目中
 *   项目--右键--build path --configure build path -- libraries --add jar 等命令引入即可
 * 
 */
public class TestDriver {

	public static void main(String[] args) {
		test05();
	}

	private static void test05() {
		// 4、组织处理业务逻辑的sql语句
		int deptno = 30;
		String strSql = "select * from emp where deptno = " + deptno;
		// 6、执行sql语句获取查询结果
		List<EmpBean> lsts = DBDriverFactory.getDBDriverInstance()
				.doQueryListBean(strSql,EmpBean.class);
		// 7、解析查询结果集
		for (EmpBean empB : lsts) {
			System.out.println(empB.toString());
		}
	}
	
	private static void test04() {
		// 4、组织处理业务逻辑的sql语句
		int deptno = 30;
		String strSql = "select * from emp where deptno = " + deptno;
		// 6、执行sql语句获取查询结果
		List<EmpBean> lsts = DBDriverFactory.getDBDriverInstance().doQueryListBean(strSql);
		// 7、解析查询结果集
		for (EmpBean empB : lsts) {
			// System.out.println(empB.toString());
			System.out.println(empB.getEname() + "--" + empB.getEmpno());
		}
	}

	private static void test03() {
		// 4、组织处理业务逻辑的sql语句
		int deptno = 30;
		String strSql = "select * from emp where deptno = " + deptno;
		// 6、执行sql语句获取查询结果
		List<Map<String, Object>> lsts = DBDriverFactory.getDBDriverInstance().doQueryListMap(strSql);
		// 7、解析查询结果集
		for (Map<String, Object> map : lsts) {
			// 遍历map对象
			Set<String> keys = map.keySet();
			for (String key : keys) {
				System.out.print("   " + key + "-" + map.get(key));
			}
			// 换行
			System.out.println();
		}

	}

	private static void test02() {
		try {
			// 4、组织处理业务逻辑的sql语句
			int deptno = 30;
			String strSql = "select * from emp where deptno = " + deptno;
			// 6、执行sql语句获取查询结果
			ResultSet rs = DBDriverFactory.getDBDriverInstance().doQuery(strSql);
			// 7、解析查询结果集
			/*
			 * ResultSet 对象具有指向其当前数据行的光标。最初，光标被置于第一行之前。 next 方法将光标移动到下一行；因为该方法在
			 * ResultSet 对象没有下一行时返回 false， 所以可以在 while 循环中使用它来迭代结果集。
			 * 
			 */
			while (rs.next()) {
				// 通过rs获取当前遍历的记录行的某一列的值,数据库记录集的下标从1开始
				System.out.println("empno-" + rs.getInt(1) + ";empname" + rs.getString("ename"));

			}
			System.out.println("再遍历一次数据库查询结果集：");
			// 重置光标到街估计第一行之前
			rs.beforeFirst();

			while (rs.next()) {
				// 通过rs获取当前遍历的记录行的某一列的值,数据库记录集的下标从1开始
				System.out.println("empno-" + rs.getInt(1) + ";empname" + rs.getString("ename"));
			}
			// 8、关闭资源
			rs.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void test01() {
		try {
			// 1、注册数据库驱动
			// 数据库驱动类型：数据库驱动类的包路径名
			String dBDriver = "oracle.jdbc.driver.OracleDriver";
			// 数据库服务器的访问连接地址：ip地址可以是localhost、127.0.0.1、ip地址
			String URL = "jdbc:oracle:thin:@10.25.128.166:1521:orcl";
			// 用户名和密码
			String userName = "scott";
			String password = "tiger";
			// Class.forName(dBDriver).newInstance();
			// 2、获取连接对象
			Connection conn = DriverManager.getConnection(URL, userName, password);
			// 3、使用连接对象创建一个查询对象
			Statement st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			// 4、组织处理业务逻辑的sql语句
			int deptno = 30;
			String strSql = "select * from emp where deptno = " + deptno;
			// 6、执行sql语句获取查询结果
			ResultSet rs = st.executeQuery(strSql);
			// 7、解析查询结果集
			/*
			 * ResultSet 对象具有指向其当前数据行的光标。最初，光标被置于第一行之前。 next 方法将光标移动到下一行；因为该方法在
			 * ResultSet 对象没有下一行时返回 false， 所以可以在 while 循环中使用它来迭代结果集。
			 * 
			 */
			while (rs.next()) {
				// 通过rs获取当前遍历的记录行的某一列的值,数据库记录集的下标从1开始
				System.out.println("empno-" + rs.getInt(1) + ";empname" + rs.getString("ename"));

			}
			System.out.println("再遍历一次数据库查询结果集：");
			// 重置光标到街估计第一行之前
			rs.beforeFirst();

			while (rs.next()) {
				// 通过rs获取当前遍历的记录行的某一列的值,数据库记录集的下标从1开始
				System.out.println("empno-" + rs.getInt(1) + ";empname" + rs.getString("ename"));
			}
			// 8、关闭资源
			rs.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}