package com.neusoft.myUtils;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.neusoft.bean.EmpBean;

/**
 * 工具类 静态方法
 * 
 * @author Administrator
 *
 */
public class MyUtil {

	/**
	 * 将resultset查询结果集转换为List+Map集合对象
	 * 
	 * @param rs
	 *            被转换的查询结果集
	 * @return list集合对象
	 */
	public static List<Map<String, Object>> convertResultSet2List(ResultSet rs) {
		// 默认返回值
		List<Map<String, Object>> lsts = new ArrayList<>();
		try {
			// 重置光标到街估计第一行之前
			rs.beforeFirst();
			// 获取ResultSetMetaData--包含查询结果集中的列信息
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				// 创建保存一行记录的map对象
				LinkedHashMap<String, Object> mapRow = new LinkedHashMap<>();
				// 遍历当前行的各列的值
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					// 获取字段名
					String strColumnName = rsmd.getColumnName(i);
					// 获取对应的字段值
					Object objColumnValue = rs.getObject(strColumnName);
					// 将各列的字段名-字段值 键值对存入map对象
					mapRow.put(strColumnName, objColumnValue);
				}
				// 将当前行的map放入list对象中
				lsts.add(mapRow);
			}
		} catch (Exception e) {
			// 将resultset查询结果集转换为List+Map集合对象异常
			System.out.println("将resultset查询结果集转换为List+Map集合对象异常：" + e.getMessage());
			e.printStackTrace();
		}

		return lsts;
	}

	/**
	 * 将resultset查询结果集转换为List+Bean集合对象 [约定：实体类Bean的属性名(小写)和数据库的字段名一样]
	 * 
	 * @param <T>
	 * @param <T>
	 * 
	 * @param rs
	 *            被转换的查询结果集
	 * @return list集合对象
	 */
	public static <T> List<T> convertResultSet2ListBean(ResultSet rs, Class<T> tClazz) {
		// 默认返回值
		List<T> lsts = new ArrayList<>();
		try {
			// 重置光标到街估计第一行之前
			rs.beforeFirst();
			// 获取ResultSetMetaData--包含查询结果集中的列信息
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				// 创建保存一行记录的map对象
				T tBean = tClazz.newInstance();
				// 遍历当前行的各列的值
				/*
				 * 在[约定：实体类Bean的属性名(小写)和数据库的字段名一样]前提下，
				 * 获取到数据库的字段名之后，也就直到了实体类的属性名； 知道了实体类的属性名，也就知道了改属性的set方法名；
				 * 默认set方法名：set+首字母大写的属性名<==>set+首字母大写的字段名
				 * 通过反射可以获取该set方法对象，然后调用invoke方法给实体类的改属性进行赋值；
				 * 这样字段的值就可以通过反射赋值给实体类中对应的属性
				 * 
				 */
				// 遍历当前行的各列的值
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					// 获取字段名---属性名
					String strColumnName = rsmd.getColumnName(i);
					// 获取对应的数据库的字段值对象
					Object objColumnValue = rs.getObject(strColumnName);
					// 因为[约定：实体类Bean的属性名(小写)和数据库的字段名一样]
					// 获取了列名也就获取了实体类的属性名
					// 将列名（属性名全部转换小写）
					String strLowerCaseName = strColumnName.toLowerCase();
					// 使用beanutils工具将赋值给指定实体bean的指定属性
					BeanUtils.setProperty(tBean, strLowerCaseName, objColumnValue);
				}
				// 将当前行的bean对象放入list对象中
				lsts.add(tBean);
			}
		} catch (Exception e) {
			// 将resultset查询结果集转换为List+Map集合对象异常
			System.out.println("将resultset查询结果集转换为List+Bean集合对象异常：" + e.getMessage());
			e.printStackTrace();
		}

		return lsts;
	}

	
	/**
	 * 将resultset查询结果集转换为List+Map集合对象
	 * 
	 * @param <T>
	 * 
	 * @param rs
	 *            被转换的查询结果集
	 * @return list集合对象
	 */
	public static List<EmpBean> convertResultSet2ListBean(ResultSet rs) {
		// 默认返回值
		List<EmpBean> lsts = new ArrayList<>();
		try {
			// 重置光标到街估计第一行之前
			rs.beforeFirst();
			// 获取ResultSetMetaData--包含查询结果集中的列信息
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				// 创建保存一行记录的map对象
				EmpBean empB = new EmpBean();
				// 遍历当前行的各列的值
				empB.setEname(rs.getString("ename"));
				empB.setEmpno(rs.getInt("empno"));
				// 其他行的转换
				// 将当前行的bean对象放入list对象中
				lsts.add(empB);
			}
		} catch (Exception e) {
			// 将resultset查询结果集转换为List+Map集合对象异常
			System.out.println("将resultset查询结果集转换为List+Map集合对象异常：" + e.getMessage());
			e.printStackTrace();
		}

		return lsts;
	}
}
